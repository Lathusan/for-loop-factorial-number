package com.four;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Jan 24, 2021
 **/

public class ForLoop {

	public static void main(String[] args) {

		int fact = 1;

		int input = 5;

		for (int i = 1; i <= input; i++) {

			fact = fact * i;

		}

		System.out.println("Factorial of " + input + " is: " + fact);

	}

}
